import React from 'react'
import { Route } from 'react-router-dom'
import jwt_decode from "jwt-decode";

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}

const distructureCookie = (props, history) => {
    let value = getCookie('Xh7ERL0G');
    const query = new URLSearchParams(props.location.search);
    const app = query.get('app');

    if (value === undefined) {
        if (app !== null) {
            window.open(`https://www.creatosaurus.io/${props.location.search}`, "_self")
        } else {
            window.open('https://www.creatosaurus.io/?app=captionator', "_self")
        }
        return false
    }
    localStorage.setItem('token', value)
    return true
}

const ProtectedRoutes = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(props) => {
        let tokenThere = distructureCookie(props)
        if(tokenThere === false) return null
        const token = localStorage.getItem('token')

        const query = new URLSearchParams(props.location.search);
        const app = query.get('app');

        //if token is not there redirect to login
        if (token === null) {
            if (app !== null) {
                window.open(`https://www.creatosaurus.io/${props.location.search}`, "_self")
            } else {
                window.open('https://www.creatosaurus.io/?app=captionator', "_self")
            }
            return
        }

        //decode the jwt token and check the expiration time
        const decoded = jwt_decode(token);
        const expirationTime = decoded.exp * 1000
        if (Date.now() >= expirationTime) {
            if (app !== null) {
                window.open(`https://www.creatosaurus.io/${props.location.search}`, "_self")
            } else {
                window.open('https://www.creatosaurus.io/?app=captionator', "_self")
            }
            return
        } else {
            return <Component {...props} />
        }
    }} />
}

export default ProtectedRoutes
