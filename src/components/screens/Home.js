import React, { useContext, useEffect } from 'react'
import '../screenscss/Home.css'
import NavigationBar from '../ReusableComponents/NavigationBar'
import LeftSideBar from '../ReusableComponents/LeftSideBar'
import CenterScreen from '../ReusableComponents/CenterScreen'
import RightSideBar from '../ReusableComponents/RightSideBar'
import AppContext from '../../store/DataProvider'
import captionTypes from '../../captionTypes'

const Home = (props) => {

    const context = useContext(AppContext)

    useEffect(() => {
        context.getUserFeatureFactoryDetails()
        const query = new URLSearchParams(props.location.search);
        const template = query.get('template');
        if (template !== null) {
            let data = captionTypes.filter(data => data.title.toLowerCase() === template.toLowerCase())
            if (data.length !== 0) {
                context.changeContentType(data[0].url)
                context.changeLeftBarActive()
                context.changeLeftSideBarActiveButton(null)
            }
        }
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <div className="home">
            <NavigationBar />
            {
                context.leftSideBarActiveButton === 2 ?
                    <div className="grid2">
                        <LeftSideBar />
                        <CenterScreen />
                    </div>
                    :
                    <div className="grid">
                        <LeftSideBar />
                        <CenterScreen />
                        <RightSideBar />
                    </div>
            }
        </div>
    )
}

export default Home
