import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../screenscss/Setting.css'

const Setting = () => {

  const context = useContext(AppContext)

  return (
    <div className='settings-container'>
      <div className='setting-toggle-container'>
        <button>Plans</button>
      </div>
      <div className='plan-details'>
        <h1>Words Detail</h1>
        <div className="plansParaOne">
          <p>Number of credit's reamining in your account</p>
          
          <button>{context.creditRemain} / {context.totalCredits}</button>
        </div>
        <button onClick={()=> window.open("https://www.creatosaurus.io/pricing")} className="upgradeBtn">Upgrade to Increase Limits</button>
      </div>
    </div>
  )
}

export default Setting