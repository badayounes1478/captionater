import React, { useContext } from 'react'
import '../screenscss/History.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AppContext from '../../store/DataProvider';
import captionTypes from '../../captionTypes';

const History = () => {

  const context = useContext(AppContext)

  const formatAMPM = (date) => {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hours + ':' + minutes + "" + ampm;
    return strTime;
  }

  const beautifyDate = (time1) => {
    time1 = parseInt(time1)
    let a = new Date(time1);
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let time = formatAMPM(a) + ' ' + date + ' ' + month + ' ' + year
    return time
  }

  const copyCaption = (data) => {
    const copyText = document.getElementById("copy");
    copyText.value = data
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    toast("Copied to clipboard", {
      autoClose: 1000,
      hideProgressBar: false,
    });
  }

  const handleScroll = (e) => {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    if (scrollTop + clientHeight >= scrollHeight - 5) {
      if (context.historyDataFinished || context.historyDataLoading) return
      context.getHistory()
    }
  }

  const generateAgain = (data) => {
    let captionDetails = data.query?.split("\n")
    const contentType = captionTypes.find((copy) => copy.title === data.copyType)

    if (data.copyType === "Freestyle") {
      context.startChat(captionDetails[0])
      context.changeLeftSideBarActiveButton(8)
      context.changeContentType(contentType.url)
      return
    }

    if (captionDetails.length === 1) {
      context.changeProductDescription(captionDetails[0])
    } else {
      context.changeProductName(captionDetails[0])
      context.changeProductDescription(captionDetails[1])
    }

    context.changeContentType(contentType.url)
    context.changeLeftSideBarActiveButton(8)

    if (captionDetails.length === 1) {
      context.generateTheCaptionsFromHistory(contentType.url, null, captionDetails[0], data.language)
    } else {
      context.generateTheCaptionsFromHistory(contentType.url, captionDetails[0], captionDetails[1], data.language)
    }
  }


  const Loader = () => {
    return <>
      {
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(data => {
          return <div className='loading-card' key={data}>
            <div className='skeleton' style={{ height: 10, width: '50%' }} />

            <div className='skeleton' style={{ height: 10, marginTop: 15 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />

            <div className='skeleton' style={{ height: 10, width: '50%', marginTop: 8 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
          </div>
        })
      }
    </>
  }

  return (
    <div className='history-center' onScroll={handleScroll}>
      <textarea id="copy" />
      {
        //check error is there or not
        context.historyDataError && context.historyDataLoading === false ?
          <div className='error-container'>
            <span>Something Went Wrong . . .</span>
            <span onClick={() => context.getHistory()}>Click here to retry</span>
          </div> :
          //check data is empty or not
          context.historyData.length === 0 && context.historyDataLoading === false ?
            <div className='empty'>You haven't created any copy yet <span onClick={() => {
              context.changeLeftBarActive()
              context.changeLeftSideBarActiveButton(null)
            }}>click here</span> to create.</div> :
            // show history data
            context.historyData.map((data, index) => {
              if (context.filterCategory === "all") {
                return <div className='card' key={data._id + index}>
                  <div className='title-container'>
                    <div className='name'>{data.copyType}</div>
                    <div className='generate' onClick={() => generateAgain(data)}>Generate Again</div>
                  </div>

                  <span>{data.query?.split("\n").map((text, index) => {
                    return <div key={text + index} style={{ textTransform: 'capitalize' }}>{text}</div>
                  })}</span>

                  {
                    data.answer.map((ans, index1) => {
                      return <div className='ans-container' key={index + "" + index1}>
                        <p style={{ display: 'flex', flexDirection: 'column' }} className='ans'>
                          <span style={{ fontWeight: 500 }}>Output {index1 + 1}</span>
                          {ans?.split("\n").map((text, index2) => {
                            return <span key={index + "" + index1 + "" + index2}>{text}</span>
                          })}
                        </p>

                        <div className='buttons-container'>
                          <svg onClick={() => copyCaption(ans)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                            <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                          </svg>

                          {
                            context.deleteId === data._id && context.deleteIndex === index1 ? <div className='loader' style={{ marginLeft: 15, height: 22, width: 22 }} /> :
                              <svg onClick={() => {
                                if (!context.canEdit) return toast("You have view permission only", {
                                  autoClose: 2000,
                                  hideProgressBar: false,
                                });
                                context.deleteHistory(data._id, index1)
                              }} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M10 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M14 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                              </svg>
                          }
                        </div>
                      </div>
                    })
                  }
                  <div className='info'>Generated by: {data.userName === undefined ? "Unknown" : data.userName}</div>
                  <div className='info' style={{ marginTop: 0 }}>Generated at: {beautifyDate(data.date)}</div>
                </div>
              } else if (data.copyType === context.filterCategory) {
                return <div className='card' key={data._id + index}>

                  <div className='title-container'>
                    <div className='name'>{data.copyType}</div>
                    <div className='generate' onClick={() => generateAgain(data)}>Generate Again</div>
                  </div>

                  <span>{data.query?.split("\n").map((text, index) => {
                    return <div key={text + index} style={{ textTransform: 'capitalize' }}>{text}</div>
                  })}</span>

                  {
                    data.answer.map((ans, index1) => {
                      return <div className='ans-container' key={index + "" + index1}>
                        <p style={{ display: 'flex', flexDirection: 'column' }} className='ans'>
                          <span style={{ fontWeight: 500 }}>Output {index1 + 1}</span>
                          {ans?.split("\n").map((text, index2) => {
                            return <span key={index + "" + index1 + "" + index2}>{text}</span>
                          })}
                        </p>

                        <div className='buttons-container'>
                          <svg onClick={() => copyCaption(ans)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                            <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                          </svg>

                          {
                            context.deleteId === data._id && context.deleteIndex === index1 ? <div className='loader' style={{ marginLeft: 15, height: 22, width: 22 }} /> :
                              <svg onClick={() => context.deleteHistory(data._id, index1)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M10 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M14 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                              </svg>
                          }
                        </div>
                      </div>
                    })
                  }
                  <div className='info'>Generated by: {data.userName === undefined ? "Unknown" : data.userName}</div>
                  <div className='info' style={{ marginTop: 0 }}>Generated at: {beautifyDate(data.date)}</div>
                </div>
              } else {
                return null
              }
            })
      }

      {
        // check data is loading or not
        context.historyDataLoading ?
          <Loader /> : null
      }
      <ToastContainer />
    </div>
  )
}

export default History