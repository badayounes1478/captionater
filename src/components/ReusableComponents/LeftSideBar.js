import React from 'react'
import LeftSideBarCommon from './LeftSideBarCommon'

const LeftSideBar = () => {
    return (
        <React.Fragment>
            <LeftSideBarCommon /> 
        </React.Fragment>
    )
}

export default LeftSideBar
