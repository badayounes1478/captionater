import React, { useContext } from 'react'
import '../ReusableComponentsCss/RightSideBar.css'
import AppContext from '../../store/DataProvider'
import RightSaved from './RightSaved'
import RightHistory from './RightHistory'
import GenerateCopy from './GenerateCopy'
import DashBoardRight from './DashBoardRight'

const RightSideBar = () => {

    const context = useContext(AppContext)

    return (
        <div className="right-side-bar">
            {
                context.leftSideBarActiveButton === 1 ? <DashBoardRight /> :
                    context.leftSideBarActiveButton === 3 ? <RightSaved /> :
                        context.leftSideBarActiveButton === 7 ? <RightHistory /> :
                            context.leftSideBarActiveButton === 8 ? <GenerateCopy /> : <DashBoardRight />
            }
        </div>
    )
}

export default RightSideBar
