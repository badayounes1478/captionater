import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../ReusableComponentsCss/LeftSideBar.css'

const LeftSideBarCommon = () => {

    const context = useContext(AppContext)

    return (
        <div className="left-side-bar">
            <div
                onClick={() => context.changeLeftSideBarActiveButton(1)}
                className={context.leftSideBarActiveButton === 1 ? "active button" : "button"}>
                <span>👀</span>
                <span>Dashboard</span>
            </div>

            <div
                onClick={() => context.changeLeftSideBarActiveButton(8)}
                className={context.leftSideBarActiveButton === 8 ? "active button" : "button"}>
                <span>🧑‍💻</span>
                <span>Generate AI Copy</span>
            </div>

            <div
                onClick={() => context.changeLeftSideBarActiveButton(2)}
                className={context.leftSideBarActiveButton === 2 ? "active button" : "button"}>
                <span>📝</span>
                <span>Templates</span>
            </div>

            <div
                onClick={() => context.changeLeftSideBarActiveButton(3)}
                className={context.leftSideBarActiveButton === 3 ? "active button" : "button"}>
                <span>💾</span>
                <span>Saved</span>
            </div>

            <div
                onClick={() => window.open('https://www.steno.creatosaurus.io/', "_blank")}
                className={context.leftSideBarActiveButton === 6 ? "active button" : "button"}>
                <span>✍🏻</span>
                <span>Editor</span>
                <svg width="22" height="23" viewBox="0 0 22 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path className="nav" d="M7.84613 14.4826L13.3704 8.95829M13.3704 8.95829V14.2616M13.3704 8.95829H8.0671" stroke="#808080" strokeLinecap="round" strokeLinejoin="round"></path>
                </svg>
            </div>

            <div
                onClick={() => context.changeLeftSideBarActiveButton(7)}
                className={context.leftSideBarActiveButton === 7 ? "active button" : "button"}>
                <span>📚</span>
                <span>History</span>
            </div>

            <div
                onClick={() => context.changeLeftSideBarActiveButton(4)}
                className={context.leftSideBarActiveButton === 4 ? "active button" : "button"}>
                <span>⚙️</span>
                <span>Settings</span>
            </div>

            <div
                onClick={() => context.changeLeftSideBarActiveButton(5)}
                className={context.leftSideBarActiveButton === 5 ? "active button" : "button"}>
                <span>💁🏻</span>
                <span>Help</span>
            </div>
        </div>
    )
}

export default LeftSideBarCommon
