import React, { useContext } from 'react'
import Google from '../../assets/Google.svg'
import Facebook from '../../assets/Facebook.svg'
import Content from '../../assets/Content.svg'
import Social from '../../assets/Social.svg'
import '../ReusableComponentsCss/DashboardCenter.css'
import AppContext from '../../store/DataProvider'

const DashboardCenter = () => {

    const context = useContext(AppContext)

    const generateCopy = (contentType) => {
        context.changeContentType(contentType)
        context.changeLeftBarActive()
        context.changeLeftSideBarActiveButton(8)
    }

    return (
        <div className="dashboard-center">
            <div className="quote-card">
                <div className="quote">
                    <span>Copy is not written. Copy is assembled.</span>
                    <span>– Eugene Schwartz</span>
                </div>
                <button onClick={() => {
                    context.changeLeftBarActive()
                    context.changeLeftSideBarActiveButton(8)
                }}>Generate AI Copy</button>
            </div>

            <div className="used-info">
                {
                    context.creditRemain === "loading" ? <span className="loader" /> :
                        <span className='trigger'>
                            {context.creditRemain}
                            <div className="tooltip">{context.totalCredits - context.creditRemain} of {context.totalCredits} words used</div>
                        </span>
                }
                <div className='trigger'>
                    words left
                    <div className="tooltip">{context.totalCredits - context.creditRemain} of {context.totalCredits} words used</div>
                </div>

                <div className="seperator"></div>

                {
                    context.createdTillNow === "loading" ? <span className="loader" /> :
                        <span className='trigger'>
                            {context.createdTillNow}
                            <div className="tooltip">You have generated {context.createdTillNow} {context.createdTillNow === 1 ? "copy" : "copies"} till now.</div>
                        </span>
                }

                <div className="trigger">
                    copy generated
                    <div className="tooltip">You have generated {context.createdTillNow} {context.createdTillNow === 1 ? "copy" : "copies"} till now.</div>
                </div>

                <div className="seperator"></div>
                {
                    context.createdTillNow === "loading" ? <span className="loader" /> :
                        <span className="trigger">
                            {context.createdTillNow * 8}
                            <div className="tooltip">You have saved {context.createdTillNow * 8} minutes by generating AI copy on creatosaurus</div>
                        </span>
                }
                <div className="trigger">
                    mins saved
                    <div className="tooltip">You have saved {context.createdTillNow * 8} minutes by generating AI copy on creatosaurus</div>
                </div>
            </div>

            <div className="copy-generator-types">
                <div className="header">
                    <span>Generate marketing copy for</span>
                    <span onClick={() => context.changeLeftSideBarActiveButton(2)}>See all</span>
                </div>

                <div className="card-container">
                    <div className="card" onClick={() => generateCopy("google/ad/headline")}>
                        <img src={Google} alt="" />
                        <div>
                            <span>Google Ad Headlines</span>
                            <span>Variations of Google ad headline copy</span>
                        </div>
                    </div>

                    <div className="card" onClick={() => generateCopy("chat")}>
                        <img src={Content} alt="" />
                        <div>
                            <span>Freestyle</span>
                            <span>Let Captionator AI answer your custom query</span>
                        </div>
                    </div>

                    <div className="card" onClick={() => generateCopy("content")}>
                        <img src={Content} alt="" />
                        <div>
                            <span>Content Rewriter</span>
                            <span>It is like a magic wand that improves your content.</span>
                        </div>
                    </div>

                    <div className="card" onClick={() => generateCopy("facebook/ad/copy")}>
                        <img src={Facebook} alt="" />
                        <div>
                            <span>Facebook Ad Copy</span>
                            <span>Variations of Facebook ad copy</span>
                        </div>
                    </div>

                    <div className="card" onClick={() => generateCopy("product/description")}>
                        <img src={Content} alt="" />
                        <div>
                            <span>Product Description</span>
                            <span>Lorem Ipsum is simply dummy text of the dummy for lorem.</span>
                        </div>
                    </div>

                    <div className="card" onClick={() => generateCopy("caption")}>
                        <img src={Social} alt="" />
                        <div>
                            <span>Social Media Captions</span>
                            <span>The best captions for your next social media post</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DashboardCenter
