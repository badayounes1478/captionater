import React, { useContext, useState } from 'react'
import '../ReusableComponentsCss/SavedCenter.css'
import AppContext from '../../store/DataProvider'
import axios from 'axios'
import constant from '../../constant'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import copyType from '../../captionTypes'

const SavedCenter = () => {

    const context = useContext(AppContext)
    const [loadingRemoveId, setloadingRemoveId] = useState(null)
    const month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

    const copyCaption = (data) => {
        const copyText = document.getElementById("copy");
        copyText.value = data
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Copied to clipboard", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const removeOne = async (id) => {
        try {
            if (!context.canEdit) return toast("You have view permission only", {
                autoClose: 2000,
                hideProgressBar: false,
            });

            const token = localStorage.getItem('token')
            setloadingRemoveId(id)
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            await axios.post(constant.url + 'saved/remove', {
                id: id
            }, config)

            toast("Deleted successfully", {
                autoClose: 1000,
                hideProgressBar: false,
            });
            setloadingRemoveId(null)
            context.removeCaptionFromSavedCaptions(id)
        } catch (error) {
            setloadingRemoveId(null)
        }
    }

    const filter = (type) => {
        let data = copyType.filter(data => data.title === type.copyType)
        if (data.length === 0) return null
        return data[0].url
    }

    const getFormatedDate = (date) => {
        date = new Date(date)
        return date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear()
    }

    const handleScroll = (e) => {
        const { scrollTop, clientHeight, scrollHeight } = e.target
        if (scrollTop + clientHeight >= scrollHeight - 5) {
            if (context.savedDataFinished || context.savedCaptionsLoading) return
            context.getSavedCaptions()
        }
    }


    const Loader = () => {
        return <>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(data => {
                    return <div className='loading-card' key={data}>
                        <div className='skeleton' style={{ height: 10, width: '50%' }} />

                        <div className='skeleton' style={{ height: 10, marginTop: 15 }} />
                        <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
                        <div className='skeleton' style={{ height: 10, marginTop: 8 }} />

                        <div className='skeleton' style={{ height: 10, width: '50%', marginTop: 8 }} />
                        <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
                        <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
                        <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
                    </div>
                })
            }
        </>
    }

    return (
        <div className="saved-center">
            <textarea id="copy" />
            {
                context.savedCaptionsError && context.savedCaptionsLoading === false ? <div className='error-container' style={{ fontSize: 16, marginTop: 10 }}>
                    <span>Something Went Wrong . . .</span>
                    <span onClick={() => context.getSavedCaptions()}>Click here to retry</span>
                </div> :
                    <div className="saved-container" onScroll={handleScroll}>
                        {
                            context.savedCaptions.length === 0 && context.savedCaptionsLoading === false ? <span style={{ display: 'block', textAlign: 'center' }}>You haven't saved anything.</span> : null
                        }
                        {
                            context.savedCaptions.map(data => {
                                if (context.savedCategory === "all") {
                                    return <div className="card" key={data._id}>
                                        <div className='head'>
                                            <div className="title">{data.copyType}</div>
                                        </div>
                                        <p style={{ display: 'flex', flexDirection: 'column' }}>{
                                            data.answer.split("\n").map((data, index) => {
                                                if (data.trim() === "") return null
                                                return <span key={index + "l"}>{data.trim()}</span>
                                            })
                                        }</p>
                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <div className="info">
                                                <span>Saved by: {data.userName === undefined ? "Unknown" : data.userName}</span>
                                                <span>Saved on: {data.createdAt === undefined ? "Not available" : getFormatedDate(data.createdAt)}</span>
                                            </div>

                                            <div style={{ display: 'flex', justifyContent: 'space-between', width: 80, alignItems: 'center' }}>
                                                <svg style={{ cursor: 'pointer' }} onClick={() => copyCaption(data.answer)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                    <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>

                                                {
                                                    data._id === loadingRemoveId ?
                                                        <div className='loader' /> :
                                                        <svg style={{ cursor: 'pointer' }} onClick={() => removeOne(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                            <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M10 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M14 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                } else if (context.savedCategory === filter(data)) {
                                    return <div className="card" key={data._id}>
                                        <div className='head'>
                                            <div className="title">{data.copyType}</div>
                                        </div>
                                        <p style={{ display: 'flex', flexDirection: 'column' }}>{
                                            data.answer.split("\n").map((data, index) => {
                                                if (data.trim() === "") return null
                                                return <span key={index + "l"}>{data.trim()}</span>
                                            })
                                        }</p>
                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <div className="info">
                                                <span>Saved by: {data.userName === undefined ? "Unknown" : data.userName}</span>
                                                <span>Saved on: {data.createdAt === undefined ? "Not available" : getFormatedDate(data.createdAt)}</span>
                                            </div>

                                            <div style={{ display: 'flex', justifyContent: 'space-between', width: 80, alignItems: 'center' }}>
                                                <svg style={{ cursor: 'pointer' }} onClick={() => copyCaption(data.answer)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                    <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>

                                                {
                                                    data._id === loadingRemoveId ?
                                                        <div className='loader' /> :
                                                        <svg style={{ cursor: 'pointer' }} onClick={() => removeOne(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                            <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M10 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M14 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                } else {
                                    return null
                                }
                            })
                        }


                        {
                            context.savedCaptionsLoading ? <Loader /> : null
                        }
                    </div>
            }
            <ToastContainer />
        </div>
    )
}

export default SavedCenter
