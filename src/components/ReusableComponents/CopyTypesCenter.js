import React, { useContext, useState } from 'react'
import '../ReusableComponentsCss/CopyTypesCenter.css'
import Facebook from '../../assets/Facebook.svg'
import Amazon from '../../assets/Amazon.svg'
import Google from '../../assets/Google.svg'
import Blog from '../../assets/Blog.svg'
import Social from '../../assets/Social.svg'
import Content from '../../assets/Content.svg'
import Youtube from '../../assets/Youtube.svg'
import Linkedin from '../../assets/LinkedIn.svg'
import Twitter from '../../assets/Twitter.svg'
import AppContext from '../../store/DataProvider'
import captionTypes from '../../captionTypes'

const CopyTypesCenter = () => {
    const context = useContext(AppContext)
    const [query, setquery] = useState("")
    const [searchResult, setsearchResult] = useState([])

    const generateCopy = (contentType) => {
        context.changeContentType(contentType)
        context.changeLeftBarActive()
        context.changeLeftSideBarActiveButton(8)
    }

    const find = (text) => {
        setquery(text)
        text = text.toLowerCase()
        text = text.split(' ');
        const data = captionTypes.filter(function (item) {
            return text.every(function (el) {
                return item.title.toLowerCase().indexOf(el) > -1 || item.subtitle.toLowerCase().indexOf(el) > -1;
            });
        });
        setsearchResult(data)
    }


    return (
        <div className='copy-type-center'>
            <form>
                <input type="text" value={query} onChange={(e) => find(e.target.value)} placeholder='Search for copy types' />
                <svg className='search' width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.5 17.5L19 21" stroke="#808080" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                    <path d="M5 13C5 16.3137 7.68629 19 11 19C12.6597 19 14.1621 18.3261 15.2483 17.237C16.3308 16.1517 17 14.654 17 13C17 9.68629 14.3137 7 11 7C7.68629 7 5 9.68629 5 13Z" stroke="#808080" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                </svg>

                {
                    query.trim().length !== 0 ?
                        <svg onClick={()=> setquery("")} className='cross' width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.0014 12.0005L17.244 17.2431M6.75879 17.2431L12.0014 12.0005L6.75879 17.2431ZM17.244 6.75781L12.0014 12.0005L17.244 6.75781ZM12.0014 12.0005L6.75879 6.75781L12.0014 12.0005Z" stroke="#808080" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg> : null
                }
            </form>
            <div className="grid">
                {
                    query.trim() === "" ? captionTypes.map((data1, index) => {
                        return <div className="card" key={index} onClick={() => generateCopy(data1.url)}>
                            <img src={data1.image === "google" ? Google : data1.image === "facebook" ? Facebook : data1.image === "amazon" ? Amazon : data1.image === "blog" ? Blog : data1.image === "social-media" ? Social : data1.image === "content" ? Content : data1.image === "youtube" ? Youtube : data1.image === "twitter" ? Twitter : data1.image === "linkedin" ? Linkedin : null} alt="" />
                            <div>
                                <span>{data1.title}</span>
                                <span>{data1.subtitle}</span>
                            </div>
                        </div>
                    }) : searchResult.map((data1, index) => {
                        return <div className="card" key={index} onClick={() => generateCopy(data1.url)}>
                            <img src={data1.image === "google" ? Google : data1.image === "facebook" ? Facebook : data1.image === "amazon" ? Amazon : data1.image === "blog" ? Blog : data1.image === "social-media" ? Social : data1.image === "content" ? Content : data1.image === "youtube" ? Youtube : data1.image === "twitter" ? Twitter : data1.image === "linkedin" ? Linkedin : null} alt="" />
                            <div>
                                <span>{data1.title}</span>
                                <span>{data1.subtitle}</span>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default CopyTypesCenter
