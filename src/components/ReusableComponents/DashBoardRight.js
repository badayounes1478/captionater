import React, { useContext, useState } from 'react'
import '../ReusableComponentsCss/DashboardRight.css'
import AppContext from '../../store/DataProvider'
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import constant from '../../constant';

const DashBoardRight = () => {

    const context = useContext(AppContext)
    const [loadingRemoveId, setloadingRemoveId] = useState(null)

    const copyCaption = (data) => {
        const copyText = document.getElementById("copy");
        copyText.value = data
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Copied to clipboard", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const removeOne = async (id) => {
        try {
            if (!context.canEdit) return toast("You have view permission only", {
                autoClose: 2000,
                hideProgressBar: false,
            });

            const token = localStorage.getItem('token')
            setloadingRemoveId(id)
            const config = { headers: { "Authorization": `Bearer ${token}` } }
            await axios.post(constant.url + 'saved/remove', {
                id: id
            }, config)

            toast("Deleted successfully", {
                autoClose: 1000,
                hideProgressBar: false,
            });
            setloadingRemoveId(null)
            context.removeCaptionFromSavedCaptions(id)
        } catch (error) {
            setloadingRemoveId(null)
        }
    }

    const Loader = () => {
        return <div className='loading-card'>
            <div className='skeleton' style={{ height: 10, width: '50%' }} />

            <div className='skeleton' style={{ height: 10, marginTop: 15 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
        </div>
    }

    return (
        <div className="dash-board-right">
            <textarea id="copy" />
            <div className="resources">
                <div className="title">Looking for a little assistance?</div>
                <a href="https://tawk.to/chat/615fde58157d100a41ab5e5d/1fhf7p2ht" target="_blank" rel="noopener noreferrer">Live chat support</a>
                <a href="https://youtu.be/eS5tpAUEuzA" target="_blank" rel="noopener noreferrer">Video Tutorial</a>
                <a href="https://www.facebook.com/groups/creatosaurus/" target="_blank" rel="noopener noreferrer">Join our Facebook group</a>
            </div>

            <div className="save">
                <div className="header">
                    <span>Recent saved</span>
                    <span onClick={() => context.changeLeftSideBarActiveButton(3)}>See all</span>
                </div>

                {
                    context.savedCaptionsError ? <div className='error-container' style={{ fontSize: 16, marginTop: 10 }}>
                        <span>Something Went Wrong . . .</span>
                        <span onClick={() => context.getSavedCaptions()}>Click here to retry</span>
                    </div> :
                        context.savedCaptionsLoading ? <div className="card-container">{
                            [1, 2, 3, 4, 5].map(data => {
                                return <Loader key={data} />
                            })
                        } </div>  :
                            <React.Fragment>
                                {
                                    context.savedCaptions.length === 0 ? <span className='blank-message'>You haven't saved anything.</span> : null
                                }
                                <div className="card-container">
                                    {
                                        context.savedCaptions.map((data, index) => {
                                            if (index >= 5) {
                                                return null
                                            } else {
                                                return <div className="card" key={data._id}>
                                                    <div className="title" style={{ textTransform: 'capitalize' }}>{data.copyType}</div>
                                                    <p style={{display:'flex', flexDirection: 'column'}}>{
                                                        data.answer.split("\n").map(data => {
                                                            if (data.trim() === "") return null
                                                            return <span key={data}>{data.trim()}</span>
                                                        })
                                                    }</p>

                                                    <div style={{ display: 'flex', justifyContent: 'space-between', width: 70, alignItems: 'center', marginTop: 5 }}>
                                                        <svg onClick={() => copyCaption(data.answer)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                            <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>

                                                        {
                                                            data._id === loadingRemoveId ?
                                                                <div className='loader' /> :
                                                                <svg onClick={() => removeOne(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path d="M10 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path d="M14 17V11" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="#000" strokeLinecap="round" strokeLinejoin="round" />
                                                                </svg>
                                                        }
                                                    </div>
                                                </div>
                                            }
                                        })
                                    }
                                </div>
                            </React.Fragment>
                }
            </div>
            <ToastContainer />
        </div>
    )
}

export default DashBoardRight
