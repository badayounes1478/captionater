import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../ReusableComponentsCss/RightApps.css'
import contentType from '../../captionTypes'

const RightApps = () => {

    const context = useContext(AppContext)

    const filter = (type) => {
        let data = contentType.filter(data => data.title === type.copyType)
        if (data.length === 0) return null
        return data[0].url
    }

    return (
        <div className="right-section-apps">
            <span className="title">Saved</span>
            {
                context.savedCaptionsError ? <div className='error-container' style={{ fontSize: 16, marginTop: 10 }}>
                    <span>Something Went Wrong . . .</span>
                    <span onClick={() => context.getSavedCaptions()}>Click here to retry</span>
                </div> : context.savedCaptionsLoading ? <span style={{ display: 'block', marginTop: 10 }}>Loading . . .</span> :
                    <React.Fragment>
                        <div>
                            <select
                                value={context.savedCategory}
                                id="copy-type"
                                name="copy-type"
                                onChange={(e) => context.changeContentTypeSaved(e.target.value)}>
                                <option value="all">All</option>
                                {
                                    contentType.map((data, index) => {
                                        return <option key={data.url + index} value={data.url}>{data.title}</option>
                                    })
                                }
                            </select>
                        </div>
                        <div className="card-container">
                            {
                                context.savedCaptions.length === 0 ? <span style={{ display: 'block', marginTop: 10 }}>You haven't saved anything.</span> : null
                            }
                            {
                                context.savedCaptions.map((data) => {
                                    if (context.savedCategory === "all") {
                                        return <div className="card" key={data._id}>
                                            <div className="title" style={{ textTransform: 'capitalize' }}>{data.copyType}</div>
                                            <p style={{ display: 'flex', flexDirection: 'column' }}>{
                                                data.answer.split("\n").map((data, index) => {
                                                    if (data.trim() === "") return null
                                                    return <span key={index + "m"}>{data.trim()}</span>
                                                })
                                            }</p>
                                        </div>
                                    } else if (context.savedCategory === filter(data)) {
                                        return <div className="card" key={data._id}>
                                            <div className="title" style={{ textTransform: 'capitalize' }}>{data.copyType}</div>
                                            <p>{
                                                data.answer.split("\n").map((data, index) => {
                                                    if (data.trim() === "") return null
                                                    return <div key={index + "m"}>{data.trim()}</div>
                                                })
                                            }</p>
                                        </div>
                                    } else {
                                        return null
                                    }
                                })
                            }
                        </div>
                    </React.Fragment>
            }
        </div>
    )
}

export default RightApps
