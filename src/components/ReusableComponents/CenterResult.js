import axios from 'axios'
import jwtDecode from 'jwt-decode'
import React, { useContext, useState, useRef } from 'react'
import constant from '../../constant'
import AppContext from '../../store/DataProvider'
import '../ReusableComponentsCss/CenterResult.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import captionTypes from '../../captionTypes'

const CenterResult = () => {
    const context = useContext(AppContext)
    const textareaRef = useRef(null);
    const [favourite, setfavourite] = useState([])
    const [loadingText, setloadingText] = useState(null)
    const [message, setMessage] = useState("")

    const addCaptionToFavorite = async (text) => {
        try {
            if (favourite.includes(text)) return toast("Already favorite", {
                autoClose: 1000,
                hideProgressBar: false,
            });

            let copyType = captionTypes.filter(data => data.url === context.contentType)
            const token = localStorage.getItem('token')
            const organizationId = localStorage.getItem('organizationId')
            const decoded = jwtDecode(token);
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            setloadingText(text)
            let res = await axios.post(constant.url + "saved", {
                organizationId: organizationId,
                userId: decoded.id,
                userName: decoded.userName,
                queryId: context.queryId,
                copyType: copyType[0].title,
                language: context.language,
                answer: text,
            }, config)

            setloadingText(null)
            setfavourite((prev) => [...prev, text])
            context.updateTheGeneratedCaptions(res.data.data)
        } catch (error) {
            setloadingText(null)
        }
    }


    const copyOne = (text) => {
        const copyText = document.getElementById("copy2");
        copyText.value = text
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Copied to clipboard", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const Loader = () => {
        return <div className='loading-card'>
            <div className='skeleton' style={{ height: 10, width: '50%' }} />

            <div className='skeleton' style={{ height: 10, marginTop: 15 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
            <div className='skeleton' style={{ height: 10, marginTop: 8 }} />
        </div>
    }

    const handleChange = (e) => {
        textareaRef.current.style.height = "0px";
        textareaRef.current.style.height = `${textareaRef.current.scrollHeight}px`;
        setMessage(e.target.value)
    }

    const startChat = async () => {
        setMessage("")
        textareaRef.current.style.height = "24px";
        context.startChat(message)
    }

    const handleKeyDown = (event) => {
        if (event.key === "Enter" && !event.shiftKey) {
            event.preventDefault();
            setMessage("")
            textareaRef.current.style.height = "24px";
            context.startChat(message)
        }
    }

    return (
        <div className="center-result" style={context.contentType === "chat" ? { paddingBottom: 65 } : null}>
            <textarea value="" id="copy2" onChange={() => console.log()} />
            <div
                style={context.chatQuestion.trim() === "" ? { display: 'none' } : null}
                className="question">
                {context.chatQuestion}
            </div>

            {
                context.captionGeneratingError !== false ? <div className='error-container'>
                    {
                        context.captionGeneratingError === 429 ? <span>
                            No Credit's Left
                        </span> : <React.Fragment>
                            <span>Something Went Wrong . . .</span>
                            <span onClick={() => context.generateTheCaptions()}>Click here to retry</span>
                        </React.Fragment>
                    }
                </div> : context.captionGerneratingLoading ? <> {
                    context.contentType === "chat" ?
                        <Loader /> :
                        [1, 2, 3].map(data => {
                            return <Loader key={data} />
                        })
                }
                </> :
                    <div className="saved-container">
                        {
                            context.generatedCaptions.length === 0 && context.contentType !== "chat" ? <div style={{ textAlign: 'center', marginTop: 20, color: '#404040', fontSize: 12 }}>Please enter the details on the right side to generate a new copy!</div> : null
                        }

                        {
                            context.generatedCaptions.map((data, index) => {
                                return <div className="card" key={index}>
                                    {
                                        data.text.split("\n").map((data, index) => {
                                            if (data.trim() === "") return null
                                            return <div key={index + "m"}>{data.trim()}</div>
                                        })
                                    }
                                    <div style={{ marginTop: 15, display: 'flex', justifyContent: 'space-between' }}>
                                        <div style={{ display: 'flex' }}>
                                            <div className='copy-button copy-button2' onClick={() => copyOne(data.text.trim())}>
                                                <span>Copy</span>
                                                <svg className='copy' xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                    <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" />
                                                </svg>
                                            </div>

                                            {
                                                loadingText === data.text.trim() ?
                                                    <div className='loader' style={{ marginLeft: 20 }} /> :
                                                    <div className='copy-button' onClick={() => addCaptionToFavorite(data.text.trim())} style={{ marginLeft: 20 }}>
                                                        <span>{favourite.includes(data.text.trim()) ? "Saved" : 'Save'}</span>
                                                        <svg className='saved' xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill={favourite.includes(data.text.trim()) ? "red" : 'none'}>
                                                            <path d="M22 8.86222C22 10.4087 21.4062 11.8941 20.3458 12.9929C17.9049 15.523 15.5374 18.1613 13.0053 20.5997C12.4249 21.1505 11.5042 21.1304 10.9488 20.5547L3.65376 12.9929C1.44875 10.7072 1.44875 7.01723 3.65376 4.73157C5.88044 2.42345 9.50794 2.42345 11.7346 4.73157L11.9998 5.00642L12.2648 4.73173C13.3324 3.6245 14.7864 3 16.3053 3C17.8242 3 19.2781 3.62444 20.3458 4.73157C21.4063 5.83045 22 7.31577 22 8.86222Z" stroke={favourite.includes(data.text.trim()) ? "red" : "#000"} strokeLinejoin="round" />
                                                        </svg>
                                                    </div>
                                            }
                                        </div>
                                        <span style={{ fontSize: 12 }}>{data.text.trim().length} Characters</span>
                                    </div>
                                </div>
                            })
                        }
                        {
                            context.generatedCaptions.length > 0 ? <div style={{ fontSize: 14, textAlign: 'center' }}>Having fun with Captionator! To generate more variety of AI content, try again with the same or a different version of your description.</div> : null
                        }
                    </div>
            }

            {
                context.contentType === "chat" ? <div className="input-container-chat">
                    <div className='text-area-wrapper'>
                        <textarea
                            value={message}
                            ref={textareaRef}
                            placeholder='Send a message...'
                            onChange={handleChange}
                            onKeyDown={handleKeyDown} />
                        <svg onClick={startChat} width="24px" height="24px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000"><g clipPath="url(#send-diagonal_svg__clip0_2476_13290)" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"><path d="M22.152 3.553L11.178 21.004l-1.67-8.596L2 7.898l20.152-4.345zM9.456 12.444l12.696-8.89"></path></g><defs><clipPath id="send-diagonal_svg__clip0_2476_13290"><path fill="#fff" d="M0 0h24v24H0z"></path></clipPath></defs></svg>
                    </div>
                </div> : null
            }
            <ToastContainer />
        </div>
    )
}

export default CenterResult
