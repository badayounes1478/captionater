import React, { useContext } from 'react'
import '../ReusableComponentsCss/CenterScreen.css'
import DashboardCenter from './DashboardCenter'
import AppContext from '../../store/DataProvider'
import SavedCenter from './SavedCenter'
import CopyTypesCenter from './CopyTypesCenter'
import CenterResult from './CenterResult'
import Setting from '../screens/Setting'
import Help from '../screens/Help'
import EditorCenter from './EditorCenter'
import History from '../screens/History'

const CenterScreen = () => {

    const context = useContext(AppContext)

    return (
        <div className="center-screen">
            {
                context.leftSideBarActiveButton === 1 ? <DashboardCenter /> :
                    context.leftSideBarActiveButton === 2 ? <CopyTypesCenter /> :
                        context.leftSideBarActiveButton === 3 ? <SavedCenter /> :
                            context.leftSideBarActiveButton === 4 ? <Setting /> :
                                context.leftSideBarActiveButton === 5 ? <Help /> :
                                    context.leftSideBarActiveButton === 6 ? <EditorCenter /> :
                                        context.leftSideBarActiveButton === 7 ? <History /> :
                                            context.changeLeftSideBarActiveButton ? <CenterResult /> : null
            }
        </div>
    )
}

export default CenterScreen
