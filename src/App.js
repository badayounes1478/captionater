import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import Home from './components/screens/Home'
import ProtectedRoutes from './components/screens/ProtectedRoutes'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <ProtectedRoutes exact path="/" component={Home} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
